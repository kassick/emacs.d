# Emacs dot files

Custom setup for GNU Emacs from a VI user

## Bootstrap

Run the bootstrap.sh script first. Then run emacs to install
everything

## Useful Packages

- Evil -- Make life bearable
    - evil-tabs
    - evil-anzu
    - evil-leader
    - evil-easymotion
    - evil-search-highlight-persist
    - evil-surround
- avy -- similar to easymotion, emacs-like
    - C-:, C-', M-g f , M-g w, M-g e
    - Used by evil-easymotion
- magit -- GIT interface for emacs
    - C-x g -- status
    - C-x M-g -- popup
- adaptive-wrap
- helm-ag -- The Silver Searcher with helm
- org-mode
    - Based on [Arnaud Legrand's .init.el](http://mescal.imag.fr/membres/arnaud.legrand/misc/init.php)
    - Some changes to revert fixes that make cua-mode conflict with evil-mode
    - Folders et al are opened with a `xdg-open` function which will run xdg-open via async-shell. This fixes many issues with file name expansion from the internal org's exec function.
- elpy -- python support
- python-docstring
- company mode -- Completer for Emacs
    - Mapped to C-c C-o   , C-c C-f, C-c C-k ... (yeah, vim C-X mode bindings on C-c)
    - ycmd/company-ycmd -- YouCompleteMe for emacs
- yasnippet -- snippets
- smex -- easyer to use execute-extended-command
- ido -- Interactive file/buffer/etc menu
- markdown-mode
- pandoc-mode
- Which-key -- show possible keybindings after a timeout
- A whitesand-based color theme (whitesandy.el)
    - Tweaked for easier to read comments, higher-contrast cursor and shiny selection on helm
- Some leaders
  - \h +PREFIX   helm something
  - \s +PREFIX   semantic things
