#!/bin/bash
# File: "bootstrap.sh"
# Created: "Seg, 20 Jun 2016 13:40:40 -0300 (kassick)"
# Updated: "2017-05-29 17:21:40 kassick"
# $Id$
# Copyright (C) 2016, Rodrigo Virote Kassick <rvkassick@inf.ufrgs.br>

git submodule update --init
pushd lisp/benchmark-init-el/
make
popd

pushd extra/jdee-server/
mvn -DskipTests=true assembly:assembly
popd

# Needed by pdftools
pkcon install poppler-devel
pkcon install poppler-glib-devel

echo "Now run emacs to install everything"
