;;; init-ido.el --- ido settings                     -*- lexical-binding: t; -*-

;; Copyright (C) 2017  Rodrigo Kassick

;; Author: Rodrigo Kassick <kassick@antagorda>
;; Keywords:

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;;

;;; Code:

(defvar ido-enable-replace-completing-read t
      "If t, use ido-completing-read instead of completing-read if possible.

    Set it to nil using let in around-advice for functions where the
    original completing-read is required.  For example, if a function
    foo absolutely must use the original completing-read, define some
    advice like this:

    (defadvice foo (around original-completing-read-only activate)
      (let (ido-enable-replace-completing-read) ad-do-it))")

;; (use-package ido
;;   :ensure t
;;   :init
;;   (setq ido-enable-flex-matching t)
;;   (setq ido-auto-merge-delay-time 1.5)
;;   :config
;;   (ido-mode 1)
;;   (ido-everywhere 1)

;;   ;; Replace completing-read wherever possible, unless directed otherwise
;;   (defadvice completing-read
;;       (around use-ido-when-possible activate)
;;     (if (or (not ido-enable-replace-completing-read) ; Manual override disable ido
;;             (and (boundp 'ido-cur-list)
;;                  ido-cur-list)) ; Avoid infinite loop from ido calling this
;;         ad-do-it
;;       (let ((allcomp (all-completions "" collection predicate)))
;;         (if allcomp
;;             (setq ad-return-value
;;                   (ido-completing-read prompt
;;                                        allcomp
;;                                        nil require-match initial-input hist def))
;;           ad-do-it))))
;;   )



;; (use-package ido-ubiquitous
;;   :ensure t
;;   :after ido
;;   :config
;;   (ido-ubiquitous-mode 1))

;; (use-package ido-vertical-mode
;;   :ensure t
;;   :after ido
;;   :config
;;   (ido-vertical-mode 1)
;;   (setq ido-vertical-define-keys 'C-n-C-p-up-down-left-right))

;; (use-package ido-hacks
;;   :ensure t
;;   :after ido
;;   )

;; (use-package flx-ido
;;   :ensure t
;;   :config
;;   (flx-ido-mode 1)
;;   (setq ido-enable-flex-matching t)
;;   )

;;; }}}

;; (use-package smex
;;   :ensure t
;;   :config
;;   (smex-initialize)
;;   (global-set-key (kbd "M-x") 'smex)
;;   (global-set-key (kbd "M-X") 'smex-major-mode-commands)
;;   ;; This is your old M-x.
;;   (global-set-key (kbd "C-c C-c M-x") 'execute-extended-command)
;; )

(defun ibuffer-ido-find-file (file &optional wildcards)
      "Like `ido-find-file', but default to the directory of the buffer at point."
      (interactive
       (let ((default-directory
               (let ((buf (ibuffer-current-buffer)))
                 (if (buffer-live-p buf)
                     (with-current-buffer buf
                       default-directory)
                   default-directory))))
         (list (read-file-name "Find file: " default-directory) t)))
      (find-file file wildcards))

(add-hook 'ibuffer-mode-hook
              (lambda ()
                (define-key ibuffer-mode-map "\C-x\C-f"
                  'ibuffer-ido-find-file)))

(provide 'init-ido)
;;; init-ido.el ends here
