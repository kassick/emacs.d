;;; init-helm.el --- Helm settings                   -*- lexical-binding: t; -*-

;; Copyright (C) 2017  Rodrigo Kassick

;; Author: Rodrigo Kassick <kassick@antagorda>
;; Keywords: lisp

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;;

;;; Code:

(use-package helm
  :ensure t
  :diminish helm-mode
  :bind (("C-x C-f" . helm-find-files)
         ("M-x"     . helm-M-x)
         ("C-x C-b" . helm-mini)
         ("C-x b"   . helm-mini)
         ("C-c h x" . helm-register)
         :map helm-map
               ("<tab>" . helm-execute-persistent-action) ; rebind tab to do persistent action
               ("C-i"   . helm-execute-persistent-action) ; make TAB works in terminal
               ("C-z"   . helm-select-action) ; list actions using C-z
               )
  :general ("hf" '(helm-find :which-key "Find Files")
            "hb" '(helm-buffers-list :which-key "Buffers")
            "ho" '(helm-occur :which-key "Occur")
            "hp" '(helm-show-kill-ring :which-key "Kill Ring")
            "sj" '(helm-semantic-or-imenu :which-key "Semantic")
            "hs" '(helm-semantic-or-imenu :which-key "Semantic")
            "hr" '(helm-resume :which-key "Resume")
           )
  :config

  (require 'helm-config)

  (global-set-key (kbd "C-c h") 'helm-command-prefix)
  (global-unset-key (kbd "C-x c")) ;; C-x c is too close to c-x c-c

  ;; General helm options
  (setq helm-move-to-line-cycle-in-source     t ; move to end or beginning of source when reaching top or bottom of source.
        helm-echo-input-in-header-line t
        ;helm-autoresize-max-height 0
        helm-autoresize-min-height 20
        helm-semantic-fuzzy-match t
        helm-imenu-fuzzy-match    t)

  (setq helm-apropos-fuzzy-match t)
  (setq helm-lisp-fuzzy-completion t)

  (defun spacemacs//helm-hide-minibuffer-maybe ()
    "Hide minibuffer in Helm session if we use the header line as input field."
    (when (with-helm-buffer helm-echo-input-in-header-line)
      (let ((ov (make-overlay (point-min) (point-max) nil nil t)))
        (overlay-put ov 'window (selected-window))
        (overlay-put ov 'face
                     (let ((bg-color (face-background 'default nil)))
                       `(:background ,bg-color :foreground ,bg-color)))
        (setq-local cursor-type nil))))


  (add-hook 'helm-minibuffer-set-up-hook
            'spacemacs//helm-hide-minibuffer-maybe)


  ;; Helm find files
  (setq helm-ff-search-library-in-sexp        t ; search for library in `require' and `declare-function' sexp.
        helm-ff-file-name-history-use-recentf t
        hhelm-ff-fuzzy-matching  nil)

  (require 'kzk-helm-hacks)
  ;; Helm-M-x
  (setq helm-M-x-fuzzy-match t) ;; optional fuzzy matching for helm-M-x

  ;; Helm-mini
  (setq helm-buffers-fuzzy-matching t
        helm-recentf-fuzzy-match    t)

  ;; Helm-registers

  ;; Enable helm mode
  (helm-autoresize-mode 1)
  (helm-mode 1)
  (diminish 'helm-mode)
)

;; {{{ Swoop -- helm search
(use-package helm-swoop
  :ensure t
  :bind (("C-s" . helm-swoop-without-pre-input)
         ("C-*" . helm-swoop)
         ("M-i" . helm-swoop)
         ("M-I" . helm-swoop-back-to-last-point)
         ("C-c M-i" . helm-multi-swoop)
         ("C-x M-i" . helm-multi-swoop-all)
         :map isearch-mode-map (("M-i" . helm-swoop-from-isearch))
         :map helm-swoop-map (("M-i" . helm-multi-swoop-all-from-helm-swoop)
                              ("M-m" . helm-multi-swoop-current-mode-from-helm-swoop)
                              ("C-r" . helm-previous-line)
                              ("C-s" . helm-next-line))
         :map evil-motion-state-map (("M-i" . helm-swoop-from-evil-search)))
  :config

  ;; Go to the opposite side of line from the end or beginning of line
  (setq helm-swoop-move-to-line-cycle t)
  )
;; }}}

(use-package helm-ag
  :ensure t
  :general
  ;; Leader mappings
  ("ha"  '(helm-ag :which-key "Ag (cwd)")
   "hd" '(nil :which-key "do")
   "hda" '(helm-do-ag :which-key "Ag")))

(provide 'init-helm)
;;; init-helm.el ends here
