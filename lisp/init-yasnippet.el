;;; init-yasnippet.el --- yasnippet settings         -*- lexical-binding: t; -*-

;; Copyright (C) 2017  Rodrigo Kassick

;; Author: Rodrigo Kassick <kassick@antagorda>
;; Keywords:

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;;

;;; Code:

(use-package yasnippet
      :ensure t
      :config
      (yas-global-mode 1)
      ;; (push '(company-semantic :with company-yasnippet) company-backends)

      ;;  (defun check-expansion ()
      ;;    (save-excursion
      ;;      (if (looking-at "\\_>") t
      ;;       (backward-char 1)
      ;;       (if (looking-at "\\.") t
      ;;         (backward-char 1)
      ;;         (if (looking-at "->") t nil)))))

      ;;  (defun do-yas-expand ()
      ;;    (let ((yas/fallback-behavior 'return-nil))
      ;;      (yas/expand)))

      ;;  (defun tab-indent-or-complete ()
      ;;    (interactive)
      ;;    (if (minibufferp)
      ;;        (minibuffer-complete)
      ;;      (if (or (not yas/minor-mode)
      ;;              (null (do-yas-expand)))
      ;;          (if (check-expansion)
      ;;              (company-complete-common)
      ;;            (indent-for-tab-command)))))

      ;; (global-set-key [tab] 'tab-indent-or-complete)

      )

;; Add yasnippet support for all company backends
;; https://github.com/syl20bnr/spacemacs/pull/179
(defvar company-mode/enable-yas nil
"Enable yasnippet for all backends.")

(defun company-mode/backend-with-yas (backend)
(if (or (not company-mode/enable-yas) (and (listp backend) (member 'company-yasnippet backend)))
        backend
      (append (if (consp backend) backend (list :sorted backend))
	  '(:with company-yasnippet))))

(eval-after-load 'company (lambda ()
                                 (setq company-backends (mapcar #'company-mode/backend-with-yas company-backends))
                                 ))


(provide 'init-yasnippet)
;;; init-yasnippet.el ends here
