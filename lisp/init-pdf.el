;;; init-pdf.el --- PDF stuff                        -*- lexical-binding: t; -*-

;; Copyright (C) 2017  Rodrigo Kassick

;; Author: Rodrigo Kassick <kassick@antagorda>
;; Keywords:

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;;

;;; Code:

;; Docview tweaks
(setq doc-view-resolution 150) ;; Keep on 150, 300 triggers https://bugzilla.redhat.com/show_bug.cgi?id=1456605 very often

(use-package pdf-tools
  :ensure t
  :config
  (pdf-tools-install)
  (eval-after-load 'evil '(add-to-list 'evil-emacs-state-modes 'pdf-annot-list-mode ))
 )

(provide 'init-pdf)
;;; init-pdf.el ends here
