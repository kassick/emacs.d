;;; init-latex.el --- LaTeX mode settings (auctex)   -*- lexical-binding: t; -*-

;; Copyright (C) 2017  Rodrigo Kassick

;; Author: Rodrigo Kassick <kassick@antagorda>
;; Keywords:

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;;

;;; Code:

;;; {{{ Latex

;; Basic settings
(use-package auctex
      :ensure t
      :mode ("\\.tex$" . LaTeX-mode)
      :commands (latex-mode LaTeX-mode plain-tex-mode)
      :general

      ;; Leader
      ("l" '(nil :which-key "LaTeX")
       "ll" '(compile :which-key "Compile Document")
       "lv" '(TeX-view :which-key "View Document")
       ;"lv" 'TeX-view
       :keymaps 'LaTeX-mode-map)

      ;; non-prefix Insert
      ((general-chord "[[") 'LaTeX-environment
       (general-chord "]]") 'LaTeX-close-environment
       :prefix nil :states 'insert :keymaps 'LaTeX-mode-map)

      :init
        ;(add-hook 'LaTeX-mode-hook #'LaTeX-preview-setup)
        ;(add-hook 'LaTeX-mode-hook #'flyspell-mode)
        (add-hook 'LaTeX-mode-hook #'turn-on-reftex)
        (add-hook 'LaTeX-mode-hook #'evil-normalize-keymaps)
        (add-hook 'tex-mode-hook #'evil-normalize-keymaps)
        (setq TeX-auto-save t
              TeX-parse-self t
              TeX-save-query nil
              TeX-PDF-mode t)
        (setq-default TeX-master nil)
        ;; (add-hook 'LaTeX-mode-hook (lambda ()
        ;;                              (evil-local-mode t)
        ;;                              (make-local-variable 'evil-leader--default-map)
        ;;                              (make-local-variable 'evil-leader--mode-maps)
        ;;                              (evil-leader/set-key
        ;;                                "l l" 'compile
        ;;                                "l v" 'TeX-view)
        ;;                              (key-seq-define evil-insert-state-local-map "[[" 'LaTeX-environment)
        ;;                              (key-seq-define evil-insert-state-local-map "]]" 'LaTeX-close-environment)
        ;;                              ))
        (setq TeX-command-default "LatexMk")
        :config
        (use-package latex-preview-pane
          :ensure t
          :config
          (latex-preview-pane-enable)
          )
      )

(use-package company-auctex
             :ensure t
             :config
             (company-auctex-init)
           )
(use-package preview
      :commands LaTeX-preview-setup
      :init
      (progn
        (setq-default preview-scale 1.4
		  preview-scale-function '(lambda () (* (/ 10.0 (preview-document-pt)) preview-scale)))))

(use-package reftex
      :commands turn-on-reftex
      :init
      (progn
        (setq reftex-plug-into-AUCTeX t)))

(use-package bibtex
      :mode ("\\.bib$" . bibtex-mode)
      :init
      (progn
        (setq bibtex-align-at-equal-sign t)
        (add-hook 'bibtex-mode-hook (lambda () (set-fill-column 120)))))


;; Auto-fill for LaTeX
(defun schnouki/latex-auto-fill ()
      "Turn on auto-fill for LaTeX mode."
      (turn-on-auto-fill)
      (set-fill-column 80)
      (setq default-justification 'left))
;     (add-hook 'LaTeX-mode-hook #'schnouki/latex-auto-fill)

;; Compilation command
(add-hook 'LaTeX-mode-hook (lambda () (setq compile-command "latexmk -pdf")))

;; Prevent ispell from verifying some LaTeX commands
;; http://stat.genopole.cnrs.fr/dw/~jchiquet/fr/latex/emacslatex
(defvar schnouki/ispell-tex-skip-alists
          '("cite" "nocite"
	"includegraphics"
	"author" "affil"
	"ref" "eqref" "pageref"
	"label" "documentclass" "usepackage"))
(setq ispell-tex-skip-alists
          (list
           (append (car ispell-tex-skip-alists)
	       (mapcar #'(lambda (cmd) (list (concat "\\\\" cmd) 'ispell-tex-arg-end)) schnouki/ispell-tex-skip-alists))
           (cadr ispell-tex-skip-alists)))

;; Indentation with align-current in LaTeX environments
(defvar schnouki/LaTeX-align-environments '("tabular" "tabular*"))
(add-hook 'LaTeX-mode-hook
	  (lambda ()
	    (require 'align)
	    (setq LaTeX-indent-environment-list
		  ;; For each item in the list...
		  (mapcar (lambda (item)
			    ;; The car is an environment
			    (let ((env (car item)))
			      ;; If this environment is in our list...
			      (if (member env schnouki/LaTeX-align-environments)
				  ;; ...then replace this item with a correct one
				  (list env 'align-current)
				;; else leave it alone
				item)))
			  LaTeX-indent-environment-list))))

;; Use dvipdfmx to convert DVI files to PDF in AUCTeX
;(eval-after-load 'tex
;      '(progn
;        (add-to-list 'TeX-command-list '("DVI to PDF" "dvipdfmx %d" TeX-run-command t t) t)
;        (add-to-list 'TeX-command-list '("LatexMk" "latexmk %t" TeX-run-command t t :help "Runs latexmk on current project") t)
;        ))

(use-package auctex-latexmk
  :ensure t
  :init
  (setq auctex-latexmk-inherit-TeX-PDF-mode t)
  :config
  (auctex-latexmk-setup))

;; SyncTeX (http://www.emacswiki.org/emacs/AUCTeX#toc19)
(defun synctex/un-urlify (fname-or-url)
      "A trivial function that replaces a prefix of file:/// with just /."
      (if (string= (substring fname-or-url 0 8) "file:///")
          (substring fname-or-url 7)
        fname-or-url))

(defun synctex/evince-sync (file linecol &rest ignored)
      "Handle synctex signal from Evince."
      (let* ((fname (url-unhex-string (synctex/un-urlify file)))
             (buf (find-buffer-visiting fname))
             (line (car linecol))
             (col (cadr linecol)))
        (if (null buf)
            (message "[Synctex]: %s is not opened..." fname)
          (switch-to-buffer buf)
          (goto-char (point-min))
          (forward-line (1- (car linecol)))
          (unless (= col -1)
            (move-to-column col)))))

(defvar *dbus-evince-signal* nil)

(defun synctex/enable-evince-sync ()
      "Enable synctex with Evince over DBus."
      (require 'dbus)
      (when (and
             (eq window-system 'x)
             (fboundp 'dbus-register-signal))
        (unless *dbus-evince-signal*
          (setf *dbus-evince-signal*
                (dbus-register-signal
                 :session nil "/org/gnome/evince/Window/0"
                 "org.gnome.evince.Window" "SyncSource"
                 'synctex/evince-sync)))))

(add-hook 'LaTeX-mode-hook 'synctex/enable-evince-sync)

(add-hook 'tex-mode-hook 'TeX-PDF-mode-on)
(add-hook 'latex-mode-hook 'TeX-PDF-mode-on)
(setq TeX-PDF-mode t)
;;; }}}


(provide 'init-latex)
;;; init-latex.el ends here
