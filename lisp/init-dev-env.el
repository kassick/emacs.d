;;; init-dev-env.el --- Dev environment and packages  -*- lexical-binding: t; -*-

;; Copyright (C) 2017  Rodrigo Kassick

;; Author: Rodrigo Kassick <kassick@antagorda>
;; Keywords:

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;;

;;; Code:

;;; Projectile support
(use-package projectile
  :ensure t
  ;;; :diminish (projectile-mode . " Ⓟ")
  )

(use-package helm-projectile
  :ensure t
  :config
  (projectile-global-mode)
  (setq projectile-completion-system 'helm)
  (helm-projectile-on)
  )

;;; Helm dash -- documentation. Must download docsets
(use-package helm-dash
  :ensure t
  :general ("doc" 'helm-dash)
  :config
  (add-hook 'c-mode-hook (lambda ()
                           (setq-local helm-dash-docsets '("C"))))
  (add-hook 'c++-mode-hook (lambda ()
                             (setq-local helm-dash-docsets '("C" "C++"))))
  (add-hook 'csharp-mode-hook (lambda ()
                                (setq-local helm-dash-docsets '("NET Framework"))))
  (setq helm-dash-docsets-path "~/.docset")
  (setq helm-dash-docsets-url "https://raw.github.com/Kapeli/feeds/master")
  (setq helm-dash-min-length 3)
  (setq helm-dash-candidate-format "%d %n (%t)")
  (setq helm-dash-enable-debugging nil)
  (setq helm-dash-browser-func 'browse-url)
  )
;; }}}

(use-package magit
  :ensure t
  :bind (("C-x g" . magit-status)
         ("C-x M-g" . magit-dispatch-popup))
  :config
  (use-package evil-magit
    :ensure t
    :init
    :config
    (evil-define-key evil-magit-state magit-mode-map "?" 'evil-search-backward)
    (add-hook 'magit-mode-hook 'evil-local-mode)
    (add-hook 'git-rebase-mode-hook 'evil-local-mode))
  )

;; {{ CEDET -- Development tools
(use-package cedet
      :ensure t
      :init
      (setq speedbar-use-images nil)            ; no imgs
      (add-hook 'speedbar-load-hook (lambda () (require 'semantic/sb)))
      ;; Hint from http://emacs.stackexchange.com/questions/600/how-to-diagnose-why-semantic-imenu-does-not-correctly-parse-a-file
      ;; Autoload wisent's java parser
      (autoload 'wisent-java-default-setup "semantic/wisent/java")
      :config
)
;; }}}
;; {{{ ecb -- class browser , project navitation
(use-package ecb
  :ensure t
  :init
  (setq ecb-tip-of-the-day nil)
  (setq ecb-options-version "2.40")
  :config
  ;; makes sure we're in emacs state so ecb will work ok with keys
  ;; et al
  (add-hook 'ecb-history-buffer-after-create-hook 'evil-emacs-state)
  (add-hook 'ecb-directories-buffer-after-create-hook 'evil-emacs-state)
  (add-hook 'ecb-methods-buffer-after-create-hook 'evil-emacs-state)
  (add-hook 'ecb-sources-buffer-after-create-hook 'evil-emacs-state)
  )
;;; }}}

(use-package flycheck
  :ensure t
  :diminish (flycheck-mode . " ⓢ")
  :evil-state (flycheck-error-list-mode . motion)
  :general
  (general-mmap "q" 'quit-window
        :keymaps 'flycheck-error-list-mode-map))

(use-package ycmd
      :ensure t
      :bind (("C-c Y r" . ycmd-reload-global-conf-file))
      :init
      (require 'ycmd-eldoc)
      (add-hook 'ycmd-mode-hook #'ycmd-eldoc-setup)
      (ycmd-eldoc-setup)
      (add-hook 'c++-mode-hook 'ycmd-mode)
      (add-hook 'c-mode-hook 'ycmd-mode)
      (add-hook 'python-mode-hook 'ycmd-mode)
      (set-variable 'ycmd-server-command
                    `("/usr/bin/python"
                      ,(expand-file-name
                        "~/.local/dev/ycm/third_party/ycmd/ycmd")))
      (set-variable 'ycmd-global-config "~/.local/dev/ycm_conf.py")
      :config
      (use-package company-ycmd
        :ensure t
        :config
        (company-ycmd-setup)
        (setq company-ycmd-insert-arguments nil))

      (use-package flycheck-ycmd
        :ensure t
        :init
        (flycheck-ycmd-setup)
        :config
        (add-hook 'c-mode-common-hook 'flycheck-mode))


      ;;; Function to reload ycm config when
      (defun ycmd-reload-global-conf-file ()
        "Tell the ycmd server to reload the global configuration file."
        (interactive)
        (ycmd-load-conf-file ycmd-global-config))
      )

;;; {{{ python support
(use-package elpy
  :ensure t
  :init
  (use-package pyvenv
    :ensure t
    ) ; WTF this dependency is not satisfied
  :config
  (setq elpy-modules '(elpy-module-eldoc
                       elpy-module-flymake
                       elpy-module-pyvenv
                       elpy-module-yasnippet
                       elpy-module-sane-defaults))
  (elpy-enable)
  )

(use-package python-docstring
        :ensure t
        :config
        (python-docstring-install)
)

;; more up-to-date python mode
;(use-package python-mode
;               :ensure t
;                :mode ("\\.py$" . python-mode)
;                :interpreter ("python" . python-mode)
;               :init
;               (setq py-complete-function nil)
;               :config
;                ;; better (auto)-formatting for python docstrings
;               (setq py-complete-function nil)
;)

(defun python-sort-completions (candidates)
      (defun py-is-priv (c)
        (equal (substring c 0 1) "_"))
      (defun my-filter (condp lst)
        (delq nil
              (mapcar (lambda (x) (and (funcall condp x) x)) lst)))
      (let* ((public (my-filter (lambda (c) (not (py-is-priv c))) candidates))
             (priv (my-filter 'py-is-priv candidates)))
        (append public priv)
      )
      )
(add-hook 'python-mode-hook
              (lambda ()
                (electric-pair-mode t)
                (setq py-auto-fill-mode t
                      py-comment-auto-fill-p t
                      py-complete-function nil)
                ;; capf with python is kind of problematic, as it sometimes sends things to the python shell and hangs up
                (make-local-variable 'company-backends)
                (make-local-variable 'company-transformers)
                (setq company-transformers '(company-sort-by-backend-importance python-sort-completions))
                (setq company-backends
                      (remove-if (lambda (k) (or (and (listp k) (member 'company-capf k))
                                                 (eq k 'company-capf)))
                                 company-backends))
                )
              )

;     }}}

;; {{{ C / C++ support
(require 'google-c-style) ; google-ish style configured in my lisp directory
(add-hook 'c-mode-common-hook
          (lambda ()
            (semantic-mode t)
            (electric-pair-mode t)
            (google-set-c-style)
            (google-make-newline-indent)
            ;; avoid company using several backends and giving headaches
            ;; (unless (eq major-mode 'java-mode)
            ;;   (set (make-local-variable 'company-backends)
            ;;        (quote (company-ycmd
            ;;                company-files
            ;;                company-keywords))))
            ))
;; }}}


;;; {{{ Java Support
;(use-package jdee
;      :ensure t
;      :config
;      (setq jdee-server-dir "~/.emacs.d/extra/jdee-server/target")
;;
;      (setq company-jdee-match-function 'company-jdee-match-fuzzy)
;      (require 'company-jdee)
;      (company-jdee-setup)
;      )

(use-package eclim
      :ensure t
      :after company
      :mode ("\\.java$" . java-mode)
      ;; :no-require t
      :config
      (setq eclimd-autostart t)
      (require 'eclimd)
      (add-hook 'java-mode-hook 'eclim-mode)
      (use-package company-emacs-eclim
        :ensure t
        :config
        (company-emacs-eclim-setup))
)

;; }}}

;;; {{{
(use-package csharp-mode
  :mode ("\\.cs" . csharp-mode)
  :ensure t
  :config

  (use-package omnisharp
    :ensure t
    :init
    (setq omnisharp-server-executable-path
          (expand-file-name "~/.local/dev/ycm/third_party/ycmd/third_party/OmniSharpServer/OmniSharp/bin/Release/OmniSharp.exe"))
    :config
    (setq omnisharp-company-match-type 'company-match-server) ; This enables server-size flex matching
    (eval-after-load 'company
      '(add-to-list 'company-backends 'company-omnisharp))
    (add-hook 'csharp-mode-hook 'omnisharp-mode))

  )

;;; {{{ Lua support
(use-package lua-mode
  :ensure t
  :mode ("\\.lua$" . lua-mode))
;;;}}}

;;; {{{
(use-package haskell-mode
  :ensure t
  :mode ("\\.hs$" . haskell-mode))
;;;}}}

;;; {{{ fsharp
(use-package fsharp-mode
      :ensure t
      :mode ( "\\.fs$" . fsharp-mode )
      :init
      (setq inferior-fsharp-program "/usr/bin/fsharpi --readline-")
      (setq fsharp-compiler "/usr/bin/fsharpc")
      (setq fsharp-ac-intellisense-enabled t)
      (setq fsharp-ac-use-popup t)
      (add-hook 'fsharp-mode-hook (lambda ()
                                    (add-to-list 'company-transformers 'company-sort-prefer-same-case-prefix)))
      )
;;;}}}

(setq auto-mode-alist (append (mapcar 'purecopy '(("\\.c$"   . c-mode)
                                                  ("\\.h$"   . c-mode)
                                                  ("\\.c.simp$" . c-mode)
                                                  ("\\.h.simp$" . c-mode)
                                                  ("\\.a$"   . c-mode)
                                                  ("\\.w$"   . cweb-mode)
                                                  ("\\.cc$"   . c++-mode)
                                                  ("\\.S$"   . asm-mode)
                                                  ("\\.s$"   . asm-mode)
                                                  ("\\.p$"   . pascal-mode)
                                                  ("\\.Rmd$" . poly-markdown-mode)
                                                  ("\\.pas$" . pascal-mode)
                                                  ("\\.tex$" . LaTeX-mode)
                                                  ("\\.txi$" . Texinfo-mode)
                                                  ("\\.el$"  . emacs-lisp-mode)
                                                  ("emacs"  . emacs-lisp-mode)
                                                  ("\\.ml[iylp]?" . tuareg-mode)
                                                  ("[mM]akefile" . makefile-mode)
                                                  ("[mM]akefile.*" . makefile-mode)
                                                  ("\\.mak" . makefile-mode)
                                                  ("\\.cshrc" . sh-mode)
                                                  ("\\.html$" . html-mode)
                                                  ("\\.py$" . python-mode)
                                                  ))
                              auto-mode-alist))

;; Require it from here, after setting all language support
(require 'init-yasnippet)

(provide 'init-dev-env)
;;; init-dev-env.el ends here
