;;; kzk-window-management.el --- Window Management   -*- lexical-binding: t; -*-

;; Copyright (C) 2017  Rodrigo Kassick

;; Author: Rodrigo Kassick <kassick@antagorda>
;; Keywords:

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:
;;; Define conveninent vim-like switch between splited windows

;;; Code:

;; Narrow
(put 'narrow-to-region 'disabled nil)
(put 'LaTeX-narrow-to-environment 'disabled nil)

;; Global keybindings
(global-set-key (kbd "C-x <up>") 'windmove-up)
(global-set-key (kbd "C-x <down>") 'windmove-down)
(global-set-key (kbd "C-x <left>") 'windmove-left)
(global-set-key (kbd "C-x <right>") 'windmove-right)
(global-set-key (kbd "C-x p") 'evil-window-mru)

(global-set-key (kbd "C-x C-<left>") 'shrink-window-horizontally)
(global-set-key (kbd "C-x C-<right>") 'enlarge-window-horizontally)
(global-set-key (kbd "C-x C-<down>") 'shrink-window)
(global-set-key (kbd "C-x C-<up>") 'enlarge-window)

;;; Target and lock window!
(use-package es-windows
  :ensure t
  :bind (("C-x 7 2" . esw/select-window)
         ("C-x 7 s" . esw/select-window)
         ("C-x 7 m" . esw/move-window)
         ("C-x 7 b" . esw/show-buffer)
         ("C-x 7 C-s" . esw/swap-two-windows)
         ("C-x 7 0" . esw/delete-window)))

(use-package popwin
  :ensure t
  :bind (("C-S-z" . popwin:keypam))
  :init
  :config
  (popwin-mode 1)
  (push '("*company-documentation*" :height 10 :position bottom :noselect t)
        popwin:special-display-config)
  (push '("^\\*Flycheck.+\\*$" :regexp t
          :dedicated t :position bottom :stick t :noselect t)
        popwin:special-display-config)
  (push '(compilation-mode :noselect t)
        popwin:special-display-config)
  )


(use-package dedicated
  :ensure t
  :bind (("C-x 9" . dedicated-mode))
  )

(use-package narrow-indirect
  ; Narrow indirect to other windows instead of cloning and then narrowing
  :ensure t
  :bind (:map ctl-x-4-map
              (("nd" . ni-narrow-to-defun-indirect-other-window)
               ("nn" . ni-narrow-to-region-indirect-other-window)
               ("np" . ni-narrow-to-page-indirect-other-window)))
  ;;; :config
  )

;;; {{{ Switch buffers
(global-set-key (kbd "<C-f10>"   ) 'ibuffer)
(global-set-key (kbd "<C-S-f10>" ) 'ibuffer-other-window)
;;; }}}

(provide 'kzk-window-management)
;;; kzk-window-management.el ends here
