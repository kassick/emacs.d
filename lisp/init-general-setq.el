;;; init-general-setq.el --- Generic setq's          -*- lexical-binding: t; -*-

;; Copyright (C) 2017  Rodrigo Kassick

;; Author: Rodrigo Kassick <kassick@antagorda>
;; Keywords:

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;;

;;; Code:


(global-set-key (kbd "C-x C-S-R") 'revert-buffer)
;; Make emacs less obstrusive
(setq inhibit-startup-screen t)
(setq tool-bar-mode -1)
(setq visible-bell 1)
;; tooltips in echo-aera
(when (and (fboundp 'tooltip-mode)
           (not (eq tooltip-mode -1)))
  (tooltip-mode -1))

;; Indentation and stuff
(setq-default indent-tabs-mode nil)
(setq tab-width 4)

(put 'scroll-left 'disabled nil)

;; Stop littering everywhere with backups and locks!
(setq backup-directory-alist '(("." . "~/tmp")))

;;; Language tweaks
(setq default-process-coding-system '(utf-8 . utf-8)) ; utf8
(set-language-environment "UTF-8")                    ; utf8
(require 'iso-transl)                                 ; see https://www.emacswiki.org/emacs/DeadKeys

;; Clear empty lines
(add-hook 'before-save-hook (lambda ()
                              (delete-trailing-whitespace)))   ; makes empty lines empty

(use-package adaptive-wrap
  :ensure t
  :config
  (add-hook 'text-mode-hook (lambda ()
                              (when (not (eq major-mode 'org-mode))
                                (progn
                                  (adaptive-wrap-prefix-mode t)
                                  (setq adaptive-wrap-extra-indent 1)))))
  )

;;; {{{ auto wrap around isearch
(defadvice isearch-search (after isearch-no-fail activate)
  (unless isearch-success
    (ad-disable-advice 'isearch-search 'after 'isearch-no-fail)
    (ad-activate 'isearch-search)
    (isearch-repeat (if isearch-forward 'forward))
    (ad-enable-advice 'isearch-search 'after 'isearch-no-fail)
    (ad-activate 'isearch-search)))
;;; }}}

;; Default modes
(line-number-mode 1)
(column-number-mode 1)
(show-paren-mode t)
(electric-pair-mode t)
(global-hl-line-mode 1)
(global-visual-line-mode)

;; {{{
;; Check if we need root to edit a file and ask for paswd
;;(defadvice find-file (after find-file-sudo activate)
;;  "Find file as root if necessary."
;;  (unless (and buffer-file-name
;;               (file-writable-p buffer-file-name))
;;    (find-alternate-file (concat "/sudo:root@localhost:" buffer-file-name))))


(use-package dash :ensure t)

(use-package sudo-edit
  :ensure t
  :bind (("C-c C-r" . sudo-edit))
  )
;; }}}


(use-package paren
                 :ensure t
                 :init
                    (show-paren-mode 1)
                    (transient-mark-mode t)
)

(use-package which-key
      :ensure t
      :diminish which-key-mode
      :config
      (setq which-key-special-keys nil
            which-key-use-C-h-for-paging t
            which-key-prevent-C-h-from-cycling t
            which-key-echo-keystrokes 0.02
                                            ; which-key-max-description-length 32
            which-key-sort-order 'which-key-key-order-alpha
            which-key-idle-delay 0.4
            which-key-allow-evil-operators t)
      (which-key-setup-side-window-bottom)
      (which-key-mode)

)

(provide 'init-general-setq)
;;; init-general-setq.el ends here
