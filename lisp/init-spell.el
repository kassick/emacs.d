;;; init-spell.el --- Spell checking                 -*- lexical-binding: t; -*-

;; Copyright (C) 2017  Rodrigo Kassick

;; Author: Rodrigo Kassick <kassick@antagorda>
;; Keywords:

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;;

;;; Code:

(defun flyspell-check-next-highlighted-word ()
      "Custom function to spell check next highlighted word"
      (interactive)
      (flyspell-goto-next-error)
      (ispell-word)
      )

(use-package flyspell
  :ensure t
  :bind (( "M-<f8>"   . flyspell-check-next-highlighted-word)
         ( "<f8>"     . ispell-word)
         ( "C-S-<f8>" . flyspell-mode)
         ( "C-M-<f8>" . flyspell-buffer)
         ( "C-<f8>"   . flyspell-check-previous-highlighted-word)
        )

  ;; :general
  ;; Motion
  ;;("]s" '(flyspell-goto-next-error :which-key "Next Misspelled Word")
  ;; :keymaps 'flyspell-mode-map :states 'motion :prefix nil)

  :diminish (flyspell-mode . " Ⓢ" ))

(use-package helm-flyspell
      :ensure t
      :general ("C-;" 'helm-flyspell-correct
                :keymaps 'flyspell-mode-map :states '(normal insert) :prefix nil
                )
      )

(provide 'init-spell)
;;; init-spell.el ends here
