;;; company-jdee.el --- Company-mode backend for JDEE  -*- lexical-binding: t; -*-

;; Copyright (C) 2016  Rodrigo Kassick

;; Author: Rodrigo Kassick <kassick@Voyager>
;; Keywords: company, jdee

;; (require 'jdee-complete)
(require 'company)
(require 's)
(require 'dash)

(defcustom company-jdee-insert-arguments t
  "When non-nil, insert function arguments as a template after completion."
  :group 'company-jdee
  :type 'boolean)

(defcustom company-jdee-match-function 'company-jdee-match-prefix
  "Which function to use to match a candidate agains the prefix"
  :group 'company-jdee
  :type '(choice
          (function-item company-jdee-match-prefix)
          (function-item company-jdee-match-fuzzy)
          (function-item company-jdee-match-exact)))

(defcustom company-jdee-match-fuzzy-minimal-lcs 2
  "How many chars must the longest-common-subsequence must have for a candidate be eligible for fuzzy match?"
  :group 'company-jdee
  :type 'integer)

(defun company-jdee--prefix ()
  "Prefix-command handler for the company backend."
  (and (eq major-mode 'jdee-mode)
       buffer-file-name
       (not (company-in-string-or-comment))
       (or (company-grab-symbol) ;-cons "\\." 1)
           'stop)))

(defun company-jdee--meta-get-candidates (prefix)
  (let* ((candidates (company-jdee--get-candidates prefix))
         ;;(candidates-non-string (cl-remove-if 'stringp candidates))
         ;;(candidates-non-string-types (map 'list (lambda (c) (format "%s" (type-of c))) candidates-non-string))
         ;;(candidates-car (format "%s" (car candidates))))
         )
    (map 'list 'company-jdee--make-candidate candidates)
    ))

(defun company-jdee--base-name-for-entry (entry)
  (let* ( (full (car entry))
          (chopped (string-trim (first (split-string full " : "))))
          (base (string-trim (first (split-string chopped "(")))))
    base))

(defun company-jdee--get-throws (extra)
  (and extra
       (string-match "throws \\(.*\\)" extra)
       (match-string 1 extra)))

(defun company-jdee--get-return (extra)
  (when extra
    (unless (string-prefix-p "throws" extra )
      (subseq extra 0 (string-match ".throws " extra)))))

(defun company-jdee--make-candidate (candidate)
  "returns the candidate without all the hassle from jdee. add information as text-properties"
  (let* ((full   (car candidate))
         (simple (cdr candidate))
         (base (company-jdee--base-name-for-entry candidate))
         (ret-maybe-throws (nth 1 (split-string full ":")))
         (full-chopped (string-trim (nth 0 (split-string full ":")) ))
         (ret-maybe-throws-trimmed (when ret-maybe-throws (string-trim ret-maybe-throws)))
         (throws (company-jdee--get-throws ret-maybe-throws-trimmed))
         (is-func (and (string-suffix-p ")" full-chopped)))
         (rtype (company-jdee--get-return ret-maybe-throws-trimmed))
         (ret (when is-func rtype))       ; functions have return type
         (type (unless is-func rtype))    ; symbols have type
         (params (and is-func
                      (string-match "\\((.*)\\)" full-chopped)
                      (match-string 1 full-chopped)))
         (doc (if is-func
                  (concat ret " "
                          base params
                          (when throws (concat " throws " throws)))
                (concat type " " base)))
         )
    (propertize base
                'return_type ret
                'meta full
                'full full
                'params params
                'type type
                'doc doc
                'throws throws )))

(defun company-jdee--meta (candidate)
  "Fetch the metadata text-property from a CANDIDATE string."
  (let ((meta (get-text-property 0 'meta candidate)))
    (if (stringp meta)
        (let ((meta-trimmed (s-trim meta)))
            meta-trimmed)
      meta)))

(defun company-jdee--simplify-params-annotation (params)
  "(java.lang.String, java.nio.Charset) -> (String, Charset)"
  (let* ((plist-text (progn
                        (string-match "(\\(.*\\))" params)
                        (match-string 1 params)))
        (plist (split-string plist-text "\\s-*,\\s-*"))
        (simplified-plist (map 'list 'company-jdee--java-chop-packages plist))
        (ret (string-join simplified-plist ", ")))
    (concat "(" ret ")" )))

(defun company-jdee--annotation (candidate)
  "Fetch the annotation text-propert from a CANDIDATE string."
  (-if-let (annotation (get-text-property 0 'annotation candidate))
      annotation
    (let ( (return-type (get-text-property 0 'return_type candidate))
           (params (get-text-property 0 'params candidate))
           (throws (get-text-property 0 'throws candidate))
           (type (get-text-property 0 'type candidate))
           )
      (concat (when (s-present? params)
                (company-jdee--simplify-params-annotation params))
              (when (s-present? return-type)
                (s-prepend " -> "
                           (company-jdee--java-chop-packages return-type)))
              (when (s-present? type)
                (concat " [" (company-jdee--java-chop-packages type ) "]"))
              (when (s-present? throws)
                (s-prepend " throws "
                           (company-jdee--java-chop-packages throws )))))))

(defun company-jdee--doc-buffer (candidate)
  "Return buffer with docstring for CANDIDATE if it is available."
  (let ((doc (get-text-property 0 'doc candidate)))
    (when (s-present? doc)
      (company-doc-buffer doc))))

(defun company-jdee--post-completion (candidate)
  "Insert function arguments after completion for CANDIDATE."
  (--when-let (and company-jdee-insert-arguments
                   (get-text-property 0 'params candidate))
    (insert it)
    (company-template-c-like-templatify
       (concat candidate it))))

(defun company-jdee--get-candidates (prefix)
  (let* ( (pair (jdee-parse-java-variable-at-point))
          jdee-parse-attempted-to-import)
    (setq company-jdee--complete-current-list nil)
    (if pair
	(condition-case err
            (setq company-jdee--complete-current-list
                  (company-jdee--complete-pair (jdee-complete-get-pair pair nil)))
	  (error
	   (condition-case err
               (let* ( (thepair (jdee-complete-get-pair pair t)) )
                 (setq company-jdee--complete-current-list (company-jdee--complete-pair thepair ))
                 )
             (error
              (message "could not complete pair: %s" (error-message-string err))))
           )))
    company-jdee--complete-current-list))

(defun company-jdee--complete-pair ( pair )
  "Returns a completion list for the pair"
  (let ((completion-list
	 (company-jdee--find-completion-for-pair pair nil (jdee-complete-get-access pair))))

    (if (null completion-list)
        (progn
          ;; Check if PREFIX is in the current class
          (setq completion-list
                (company-jdee--find-completion-for-pair (list
                                                         (concat "this." (car pair)) "")
                                                        nil
                                                        jdee-complete-private))))
    ;;if completions is still null check if the method is in the
    ;;super class
    (if (null completion-list)
        (setq completion-list (company-jdee--find-completion-for-pair
                                 (list (concat "super." (car pair)) "")
                                 nil jdee-complete-protected)))

    (if (null completion-list) (error "No completions at point111"))

    completion-list
    ))

(defun company-jdee--find-completion-for-pair (pair &optional exact-completion access-level)
  "PAIR is (PREFIX PARTIAL). EXACT-COMPLETION is nil or non-nil.
ACCESS-LEVEL is one of: `jdee-complete-private'
`jdee-complete-protected' nil. Return a list of possible
completions from beanshell."
  (let ((type (jdee-parse-eval-type-of (car pair))))
    (if type
	(cond
	 ((member type jdee-parse-primitive-types)
	  (error "Cannot complete primitive type: %s" type))

	 ((string= type "void")
	  (error "Cannot complete return type of %s is void." (car pair)))

	 (access-level
	  (let ((classinfo (jdee-complete-get-classinfo type access-level)))
	    ;; FIXME: when is classinfo nil?
	    (when classinfo
              (setq company-jdee--complete-current-list
                    (company-jdee--find-all-completions
                     pair classinfo type exact-completion)))))

	 (t
	  (let ((classinfo (jdee-complete-get-classinfo type)))
	    ;; FIXME: when is classinfo nil?
	    (when classinfo
		(setq company-jdee--complete-current-list
		      (company-jdee--find-all-completions
		       pair classinfo type exact-completion))))))

      ;; type is nil
      nil)))


;;; match functions
;;; receive a prefix (substring being completed)
;;; an entry, type cons cell ("nameOfFunction(type1 p1, type2 p2, ...) : rtype throws Exception)" . "nameOfFunction")
(defun company-jdee--prefix-match-map (prefix base entry)
  "Matches by prefix: prefix='get'  regurns getChars, getCharAt, ... etc"
  (let* ((match (string-match prefix base)))
    (when (equal 0 match) 0)))

(defun company-jdee-match-prefix ()
  "Prefix match"
  (cons 'company-jdee--prefix-match-map nil))


(defun company-jdee--exact-match-map (prefix base entry)
  (let* ((match (string= prefix base)))
    (when match 0)))

(defun company-jdee-match-exact ()
  "Exact match"
  (cons 'company-jdee--exact-match-map nil))


;; ``static'' char table used by match fuzzy
;; Fuzzy matching would be faster with 2 bool vectors -- index by char, set each occurence to 1 in candidate and prefix, do a bitwise-and and count occurences in 1.
;; BUT java allows unicode chars in identifiers. A bitmap for UTF-16 would take 8K, &-ing two of these would be slow.
;; Better option would be to have a sparse structure that holds bitmaps for ranges of chars (i.e most entries would be between 30-255). Not in the mood to create such thing. right now

(setq company-jdee--fuzzy-match-ct (make-char-table 'bool nil))

(defun company-jdee--fuzzy-filter (lst)
  "Will remove candidates that don't have at lest as many common characters with prefix as the best candidate"
  (when lst
    (let* ( (best (car lst))
            (score-best (elt best 0))
            (lcs-score-best (elt score-best 0))
            (lcs-start-best (elt score-best 1))
            (ncommon-best   (elt score-best 2))
            )
      (remove-if-not
       (lambda (candidate)
         (let* ((score (elt candidate 0))
                (lcs-score (elt score 0))
                (lcs-start (elt score 1))
                (ncommon   (elt score 2))
                )
           (or (<= lcs-score lcs-score-best)
               (<= ncommon lcs-score-best))))
       lst))))

(defun company-jdee--match-fuzzy-is-valid-candidate (prefix candidate lcs lcs-start ncommon)
  (and (> ncommon 1)
       (>= lcs company-jdee-match-fuzzy-minimal-lcs)))

(defun company-jdee--match-fuzzy-reduce-ct (ct prefix candidate)
  "Sums how many chars from candidate are true on the ct and finds the common longest subsequence. Returns nil or a vector"

  (let ((ncommon 0)
        (lcs 0)
        (lcs-start nil)
        (lcs-tmp-start nil)
        (curs 0))
    (dotimes (i (length candidate))
      (let ( (c (elt candidate i)))
        (if (aref ct c)
            (progn
              (incf ncommon)
              (incf curs)
              (if (null lcs-tmp-start)
                  (setq lcs-tmp-start i))
              (when (> curs lcs)
                (setq lcs curs)
                (setq lcs-start lcs-tmp-start)
                ))
          (progn
            (setq curs 0)
            (setq lcs-tmp-start nil))
          )))

    (when (company-jdee--match-fuzzy-is-valid-candidate prefix
                                                        candidate
                                                        lcs
                                                        lcs-start
                                                        ncommon)
      ;; at least 2 common letters, otherwise it's too forgiving
      (vector (- lcs) lcs-start (- ncommon)))))

(defun company-jdee--match-fuzzy-score (prefix base)
  "Update the ct"
  ;; clear the static char table
  (set-char-table-range company-jdee--fuzzy-match-ct t nil)

  ;; sets every char in prefix to true in the fuzzy ct
  (mapc (lambda (c)
          (aset company-jdee--fuzzy-match-ct c t)
          )
        prefix)
  ;; Returns how many chars we have set that are common in base
  (company-jdee--match-fuzzy-reduce-ct company-jdee--fuzzy-match-ct
                                       prefix
                                       base))

(defun company-jdee--fuzzy-map (prefix base entry)
  "Mathes by letter match count: prefix='get' returns getChars, getCharAt, GetChar, CharGet, Chaleirageldat
This is YCMD style fuzzy match: returns any entry that contains ALL chars, ordered by how many chars did match"
  (if (s-present? prefix)
      (let* ((upbase (upcase base))
             (upprefix (upcase prefix)) )
        (company-jdee--match-fuzzy-score upprefix upbase))
    (vector 0 0 0))   ; Searching empty prefix, all entries are equal
  )

(defun company-jdee-match-fuzzy ()
  "Fuzzy Match"
  (cons 'company-jdee--fuzzy-map 'company-jdee--fuzzy-filter))

(defun company-jdee--find-matches (pred prefix lst exact-match)
  "Filters entries in lst matching prefix. Will use either company-jdee-match-exact or the value of company-jdee-match-function. The match functions return either nil or a (cons entry rating). The rating must be comparable by a sort function"
  (let (matches)
    (dolist (cur lst matches)
      (let* ( (base (company-jdee--base-name-for-entry cur))
              (result (funcall pred prefix base cur))
              (lresult (when result (list (vector result base cur)))))
        ;;; Append [base result cur] to the result list
        (setq matches (append matches lresult))))))

(defun company-jdee--safe-compare (e1 e2)
  "compares maybe tuples maybe vectors maybe cthulhu"
  (cond ( (or (null e1) (null e2))
          (if (not (null e2))
              nil         ; nil < 1 => false
            (if (not (null e1 ))
                t         ; 1 < nil => true
              nil         ; nil < nil => false
              )))
        ( (stringp e1) (string-lessp e1 e2))
        ( (numberp e1) (< e1 e2))
        ( (consp e1)
          (let ( (ca1 (car e1) )
                 (ca2 (car e2) )
                 (cr1 (cdr e1) )
                 (cr2 (cdr e2) ))
            (if (equalp ca1 ca2) (company-jdee--safe-compare cr1 cr2)
              (company-jdee--safe-compare ca1 ca2))))
        ( (vectorp e1)
          ;; compare each element. Stop when smaller
          (let* (result
                 stop)
            (dotimes (i (length e1) )
                     (when (not stop)
                       (let* ( (v1 (elt e1 i) )
                               (v2 (elt e2 i) ))
                         (when (not (equalp v1 v2))
                           (setq result (company-jdee--safe-compare v1 v2))
                           (setq stop t)))))
            result))
        ( t (error "CTHULHU Can not compare %s with %s" (type-of e1) (type-of e2)))))

(defun company-jdee--java-chop-packages (type)
  (if type
      (car (last (split-string type "\\.")))
    ""))

(defun company-jdee--entry-is-ctor-p (ctor-name entry)
  (let* ((base (elt entry 1)))
    (string= ctor-name base)))

(defun company-jdee--find-all-completions (pair lst type &optional exact-match)
  "Finds all completions for a pair. If the text searched is not the name of the type, do not include constructors"
  (let* (tmp
	 chop-pos
	 (args (nth 2 pair))
	 (pat (nth 1 pair))
         (ctor-name (company-jdee--java-chop-packages type)))

    (if (null args)
	(setq exact-match nil)
      (setq pat (concat pat args)))

    ;;Get all results, sort them by safe-compare. Since we add vectors to the return list, on the form [basename rank (full . simple)], safe-compare will sort mostly on basename and rank

    (let* ( (pred-functor (if exact-match
                              'company-jdee-match-exact
                            company-jdee-match-function))
            (pred-cons (funcall pred-functor))
            (pred (car pred-cons))
            (candidate-filter (cdr pred-cons))
            results)

      (setq results (company-jdee--find-matches pred pat lst exact-match))
      (setq results (sort results 'company-jdee--safe-compare))
      (when candidate-filter
        (setq results (funcall candidate-filter results)))

      ;; get only the original cons (full . simple)
      (map 'list
           (lambda (e) (elt e 2))
           (if (not (string-equal pat ctor-name))
               ;; Looking for something OTHER then the ctor
               (remove-if (lambda (e)
                            (company-jdee--entry-is-ctor-p ctor-name e))
                          results)
             results)))))


(defun company-jdee (command &optional arg &rest ignored)
  "Define a backend for company mode using jdee"
  (interactive (list 'interactive))
  (case command
    (interactive (company-begin-backend 'company-jdee))
    ;; what is the prefix command for company backend?
    (prefix (company-jdee--prefix))
    (meta            (company-jdee--meta arg))
    (annotation      (company-jdee--annotation arg))
    (post-completion (company-jdee--post-completion arg))
    (doc-buffer      (company-jdee--doc-buffer arg))
    (sorted          't)
    (candidates (company-jdee--meta-get-candidates arg))
  ))

(defun company-jdee-setup ()
  (add-to-list 'company-backends 'company-jdee))

(provide 'company-jdee)
