;;; Original from Manuel Uberti https://github.com/manuel-uberti/.emacs.d/blob/db9f92cc63a160560ef692d1878819b20c3bf484/lisp/mu-style.el
;;; Stealing some tricks


;;; mu-style.el --- Part of my Emacs setup -*- lexical-binding: t; -*-

;; Copyright (C) 2016  Manuel Uberti

;; Author: Manuel Uberti manuel.uberti@inventati.org
;; Keywords: convenience

;;; Commentary:

;; This file stores my look'n'feel configuration.

;;; Code:

;;; Fonts setup
;; Fonts used:
;; - Source Code Pro (https://github.com/adobe-fonts/source-code-pro)
;; - Fira Sans (https://github.com/mozilla/Fira/)
(defun mu-setup-main-fonts (default-height variable-pitch-height)
  "Set up default fonts.

Use DEFAULT-HEIGHT for default face and VARIABLE-PITCH-HEIGHT
for variable-pitch face."
  (set-face-attribute 'default nil
                      :family "Source Code Pro"
                      :height default-height)
  (set-face-attribute 'variable-pitch nil
                      :family "Fira Sans"
                      :height variable-pitch-height
                      :weight 'regular))

(defun kzk/adjust-font-size ()
  ;; Dinamically change font size based upon screen resolution
  (if (display-graphic-p)
      (if (> (display-pixel-width) 1800) ; Has X, query pixel width
          (mu-setup-main-fonts 130 140)
        (mu-setup-main-fonts 120 130))
    (mu-setup-main-fonts 130 140) ; no X yet, maybe we are a server starting?
                                  ; Default to 130 140
    )
  )

(add-hook 'before-make-frame-hook 'kzk/adjust-font-size)
(kzk/adjust-font-size)

;;; Color scheme

;;; (load-theme 'white-sandy t) ; my take on white sand

(setq custom-safe-themes t)    ; Treat themes as safe
(use-package color-theme
                 :ensure t)

(use-package ample-theme
  :init (progn (load-theme 'ample t t)
               (load-theme 'ample-flat t t)
               (load-theme 'ample-light t t)
               (enable-theme 'ample))
  :config
  (custom-set-faces '(region ((t (:background "gray31"))))
                    '(helm-ff-dotted-directory ((t (:foreground "#6aaf50" :weight bold))))
                    '(helm-ff-dotted-symlink-directory ((t (:foreground "DarkOrange" :weight bold)))))
  :defer t
  :ensure t)

;; (setq frame-background-mode 'dark)
;; (set-frame-parameter nil 'background-mode 'dark)
;; (set-terminal-parameter nil 'background-mode 'dark)
;; (use-package color-theme-solarized
;;   :init
;;   (setq frame-background-mode 'dark)
;;   :ensure t
;;   :config
;;   (load-theme 'solarized 'no-confirm)
;;   (custom-set-faces
;;    '(header-line ((t (:background "#002b36" :foreground "#eee8d5" :inverse-video nil :weight bold :inherit nil))))
;;    '(helm-header ((t (:background "#002b36" :foreground "#2aa198" :slant italic :inherit nil))))
;;    '(helm-selection ((t
;;                       ( :background "#073642"
;;                         :weight bold
;;                         :foreground "#859900"
;;                         :underline "#859900"
;;                         :inherit nil))))
;;    )
;;   (add-hook 'after-make-frame-functions
;;           (lambda (frame)
;;             (let ((mode (if (display-graphic-p frame) 'dark 'dark)))
;;               (set-frame-parameter frame 'background-mode mode)
;;               (set-terminal-parameter frame 'background-mode mode))
;;             (enable-theme 'solarized)))
;;   )



;; (use-package solarized-theme                  ; Default theme
  ;; :ensure t
  ;; :config
  ;; (setq
   ;; solarized-use-variable-pitch nil  ; Disable variable pitch fonts
   ;; solarized-distinct-doc-face t     ; Make doc faces stand out more
   ;; ;; solarized-use-more-italic t       ; Use italic more often
   ;; ;; solarized-use-less-bold t         ; Less bold, italic is enough
   ;; ;; Avoid all font-size changes
   ;; ;; solarized-height-minus-1 1.0
   ;; ;; solarized-height-plus-1 1.0
   ;; ;; solarized-height-plus-2 1.0
   ;; ;; solarized-height-plus-3 1.0
   ;; ;; solarized-height-plus-4 1.0
   ;; )
;;
   ;; (load-theme 'solarized-dark 'no-confirm)
   ;; )
;;

;; Helpers
;; unbind annoying shortcuts that I don't use
(unbind-key "C-z")
(unbind-key "C-x C-z")
(unbind-key "C-x C-c")

(setq echo-keystrokes 0.1)              ; Faster echo keystrokes

;; Avoid showing ?? in the mode line when we have long lines.
(setq line-number-display-limit-width 10000)

;; Turn off annoying settings
(blink-cursor-mode -1)
(tooltip-mode -1)

;; Disable annoying prompts
(fset 'yes-or-no-p 'y-or-n-p)
(setq kill-buffer-query-functions
               (remq 'process-kill-buffer-query-function
                     kill-buffer-query-functions))

;; Disable startup messages
(setq ring-bell-function #'ignore
               inhibit-startup-screen t
               initial-scratch-message nil)

;; Disable startup echo area message
(fset 'display-startup-echo-area-message #'ignore)

(setq history-length 1000)           ; Store more history
(setq-default line-spacing 0.2)         ; Increase line-spacing (default 0)

;; Configure a reasonable fill column and enable automatic filling
(setq-default fill-column 80)
;;(add-hook 'text-mode-hook #'auto-fill-mode)
;;(diminish 'auto-fill-function)

;; Give us narrowing back!
(put 'narrow-to-region 'disabled nil)
(put 'narrow-to-page 'disabled nil)
(put 'narrow-to-defun 'disabled nil)

;; Same for region casing
(put 'upcase-region 'disabled nil)
(put 'downcase-region 'disabled nil)

(use-package page-break-lines           ; Better looking break lines
  :ensure t
  :defer t
  :init (global-page-break-lines-mode)
  :diminish page-break-lines-mode)

;;; Prettify symbols
(global-prettify-symbols-mode 1)

;; Unprettify symbols with point on them and symbols
;; right next to point
(setq prettify-symbols-unprettify-at-point 'right-edge)

(use-package ansi-color                 ; Colorize ANSI escape sequences
  :defer t
  :config
  (defun mu-colorize-compilation ()
    "Colorize from `compilation-filter-start' to `point-max'."
    (let ((inhibit-read-only t))
      (ansi-color-apply-on-region compilation-filter-start (point-max))))

  (add-hook 'compilation-filter-hook #'mu-colorize-compilation))

;; Underline below the font bottomline instead of the baseline
(setq x-underline-at-descent-line t)

;(use-package stripe-buffer              ; Add stripes to a buffer
;  :ensure t
;  :init
;  (add-hook 'dired-mode-hook 'stripe-listify-buffer)
  ;(add-hook 'org-mode-hook 'turn-on-stripe-table-mode)
;  )



;;; The mode line
;;(line-number-mode)
;;(column-number-mode)

(provide 'mu-style)

;; Local Variables:
;; coding: utf-8
;; indent-tabs-mode: nil
;; End:

;;; mu-style.el ends here
