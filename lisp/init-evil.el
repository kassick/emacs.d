;;; init-evil.el --- Initializes evil et al          -*- lexical-binding: t; -*-

;; Copyright (C) 2017  Rodrigo Kassick

;; Author: Rodrigo Kassick <kassick@antagorda>
;; Keywords:

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;;

;;; Code:

(setq evil-toggle-key "C-M-S-z") ; get this key out of the way!
(setq evil-symbol-word-search t)
(setq-default evil-cross-lines t)

(defcustom kzk/leader "\\"
  "Default <leader> to use

Use the following prefix as default <leader> on general.el bindings"

  :group 'kzk
  :type 'string)

(defun minibuffer-keyboard-quit ()
        "Abort recursive edit.
        In Delete Selection mode, if the mark is active, just deactivate it;
        then it takes a second \\[keyboard-quit] to abort the minibuffer."
      (interactive)
      (if (and delete-selection-mode transient-mark-mode mark-active)
          (setq deactivate-mark  t)
        (when (get-buffer "*Completions*") (delete-windows-on "*Completions*"))
        (abort-recursive-edit)))

(use-package key-chord
  :ensure t
  :config
  (key-chord-mode 1)
  )
(use-package general
  :after key-chord
  :ensure t
  :config
  (general-evil-setup)
  (setq general-default-states '(normal visual)
        general-default-prefix kzk/leader)

  (general-define-key :prefix kzk/leader :keymaps 'normal
                      ;; "unbinds" the leader
                      "" '(nil :which-key "<leader>")
                      "h" '(:ignore t :which-key "Helm")
                      "c" '(:ignore t :which-key "Compilation")
                      "d" '(:ignore t :which-key "Documentation")
                      "s" '(:ignore t :which-key "Semantic")
                      )

  (general-define-key "cc" 'compile
                      "cl" (lambda () (interactive)
                             (compile compile-command)))
  )


;; Add :evil-leader, :evil-bind and :evil-state
(require 'evil-use-package)

;;; }}}  evil-leader

;;; {{{ Evil
(use-package evil
  :ensure t
  :demand t

  :general

  ;; Normal-state window movements
  ("C-w <up>"    '(windmove-up :which-key "Window Up")
   "C-w <down>"  '(windmove-down :which-key "Window Down")
   "C-w <left>"  '(windmove-left :which-key "Window Left")
   "C-w <right>" '(windmove-right :which-key "Window Right")
   :prefix nil :states '(normal visual)
   )

  :init
  (evil-mode +1)
  :config
  ;; Make movement keys work like they should
    (define-key evil-normal-state-map (kbd "<remap> <evil-next-line>") 'evil-next-visual-line)
    (define-key evil-normal-state-map (kbd "<remap> <evil-previous-line>") 'evil-previous-visual-line)
    (define-key evil-motion-state-map (kbd "<remap> <evil-next-line>") 'evil-next-visual-line)
    (define-key evil-motion-state-map (kbd "<remap> <evil-previous-line>") 'evil-previous-visual-line)
    (define-key evil-normal-state-map (kbd "j") 'evil-next-visual-line)
    (define-key evil-normal-state-map (kbd "k") 'evil-previous-visual-line)
                                        ; spell movements
    ; (define-key evil-normal-state-map (kbd "]s") 'flyspell-goto-next-error)
    ;; esc quits pretty much anything (like pending prompts in the minibuffer)
    (define-key evil-normal-state-map [escape] 'keyboard-quit)
    (define-key evil-visual-state-map [escape] 'keyboard-quit)
    (define-key evil-normal-state-map (kbd "-") (lambda () (interactive) (dired ".")))
    (define-key minibuffer-local-map [escape] 'minibuffer-keyboard-quit)
    (define-key minibuffer-local-ns-map [escape] 'minibuffer-keyboard-quit)
    (define-key minibuffer-local-completion-map [escape] 'minibuffer-keyboard-quit)
    (define-key minibuffer-local-must-match-map [escape] 'minibuffer-keyboard-quit)
    (define-key minibuffer-local-isearch-map [escape] 'minibuffer-keyboard-quit)
    (define-key evil-insert-state-map (kbd "C-v") 'yank)

    (evil-define-key 'normal global-map (kbd "Z W") 'quit-window)

    ;;; Do not close the whole tab on :q !!!!!
    ;; (evil-ex-define-cmd "q[uit]" 'evil-quit)
  )

;;; }}} evil

;;; Anzu: Show number of hits for current ongoing search (bottom left corner of window)
(use-package evil-anzu
             :ensure t
             :after evil
             :config
             (global-anzu-mode)
             )

;; matchit: Use % to navigate on the structure (i.e. if / else , try/excepts ... etc)
(use-package evil-matchit
             :ensure t
             :after evil
             :config
             (global-evil-matchit-mode 1)
)

;; EasyMotion for Evil
(use-package evil-easymotion
  :after evil
  :ensure t
  :config
  (evilem-default-keybindings kzk/leader)
)

;; Emulate evil tabs
(use-package evil-tabs
  :ensure t
  :after evil
  :bind (("C-<next>" . elscreen-next)
         ("C-<prior>" . elscreen-previous))
  :config
  (global-evil-tabs-mode)
  )

;; Make ediff more evil
(use-package evil-ediff
  :ensure t
  :after evil
  )

;; Keep the highlight after the search
(use-package evil-search-highlight-persist
  :ensure t
  :after evil
  :config
  (global-evil-search-highlight-persist)
  )

;; Evil surround
(use-package evil-surround
  :after evil
  :ensure t
  :config
  (global-evil-surround-mode)
  )

;; Evil embrace
(use-package evil-embrace
  :ensure t
  :after evil-surround
  :config
  (add-hook 'org-mode-hook 'embrace-org-mode-hook)
  (evil-embrace-enable-evil-surround-integration))

;; Visual star: hit * or # to start a search using the selection
(use-package evil-visualstar
  :ensure t
  :after evil
  :config
  (global-evil-visualstar-mode)
  )

;; Make indent a text object
(use-package evil-indent-plus
  :after evil
  :ensure t
  :config
  (evil-indent-plus-default-bindings)
  )

;; Add b textobj -- any block surrounded by () , {} or whatever
(use-package evil-textobj-anyblock
  :ensure t
  :after evil
  :config
  (define-key evil-inner-text-objects-map "A" 'evil-textobj-anyblock-inner-block)
  (define-key evil-outer-text-objects-map "A" 'evil-textobj-anyblock-a-block)
  )

;; Make arguments a text object
(use-package evil-args
  :ensure t
  :after evil
  :general

  ;; Args movements
  ("L" '(evil-forward-arg :which-key "Arg Forward")
   "H" '(evil-backward-arg :which-key "Arg Backward")
   "K" '(evil-jump-out-args :which-key "Jump Out of Args")
   :prefix nil :states 'normal)

  :config
  (define-key evil-inner-text-objects-map "a" 'evil-inner-arg)
  (define-key evil-outer-text-objects-map "a" 'evil-outer-arg)
  )

;; bind evil-forward/backward-args

(use-package evil-vimish-fold
  :ensure t
  :diminish evil-vimish-fold-mode
  :config
  (evil-vimish-fold-mode 1)
  )

;; Correct mapping of key sequences
(use-package key-seq
  :ensure t
  :after evil
  :config
  (key-chord-mode 1)
  )

;;; {{{ Insert filenames in the minibuffer
;;;     Lame substitute for % but well, better than nothing
;;;     maps C-c f  -> current filename
;;;          C-c C-f -> Pick a filename and insert it
;;;     C-u prefix : Inserts the file with full path

;;; Inserts the current file name
(defun insert-filename-or-buffername (&optional arg)
  "If the buffer has a file, insert the base name of that file.
  Otherwise insert the buffer name.  With prefix argument, insert the full file name."
  (interactive "P")
  (let* ((buffer (window-buffer (minibuffer-selected-window)))
         (file-path-maybe (buffer-file-name buffer)))
    (insert (if file-path-maybe
                (shell-quote-argument
                        (if arg
                            file-path-maybe
                          (file-relative-name file-path-maybe)))
              (buffer-name buffer)))))

;;; Picks a file name and inserts it. Needs recursive minibuffers
;;; (because of ido, probably standard read-file-name as well)
(defun insert-filename-from-read-file (&optional arg)
  (interactive "P")
  (let* ((enable-recursive-minibuffers t)
         (file-path-maybe (read-file-name "File: ")))
    (insert (if file-path-maybe
                (shell-quote-argument
                        (if arg
                            file-path-maybe
                        (file-relative-name file-path-maybe)))
              ""))))

(define-key minibuffer-local-map (kbd "C-c f") 'insert-filename-or-buffername)
(define-key minibuffer-local-map (kbd "C-c C-f") 'insert-filename-from-read-file)
;;; }}}

(provide 'init-evil)
;;; init-evil.el ends here
