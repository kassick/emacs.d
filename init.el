;;; init.el
;;; Kassick's evil emacs conf

(setq gc-cons-threshold 100000000)

;; {{{ Benchmark the loading of packages
;; (add-to-list 'load-path "~/.emacs.d/lisp/benchmark-init-el/")
;; (require 'benchmark-init-loaddefs)
;; (benchmark-init/activate)
;; }}}

;; {{{ Custom File -- out of init, stop messing with VC
(let ((custom (expand-file-name "~/.emacs.d/custom.el")))
  (unless (file-exists-p custom)
    (write-region "" nil custom))
  (setq custom-file custom)
  )
;; }}}

;; {{{ Package repos and use-package stuff
(require 'package)
(setq package-enable-at-startup nil)
(package-initialize)
(add-to-list 'package-archives '("melpa" . "http://melpa.org/packages/")) ;; remove temporaraily
;; (add-to-list 'package-archives '("marmalade" . "http://marmalade-repo.org/packages/")) ;; add temporareily
(add-to-list 'package-archives '("org" . "http://orgmode.org/elpa/") )
(unless (package-installed-p 'use-package)
    (package-refresh-contents)
    (package-install 'use-package))
(eval-when-compile
  (require 'use-package)
  ;;; (setq use-package-always-defer t) ;; may make it faster, but must bind a lot of stuff
  )

(require 'bind-key)

(use-package validate
  :ensure t)


(add-to-list 'load-path (expand-file-name "~/.emacs.d/lisp"))

(require 'nifty) ; Some useful functions from someone's github ...
(require 'xclip) ; Integrate with xclip, so copy/paste works on the terminal too
(require 'init-general-setq)
(require 'mu-style)
(require 'init-diminish)
(require 'init-recentf)
(require 'init-evil)
(require 'init-midnight)
(require 'init-helm)
; (require 'init-ido)
(require 'kzk-headers)
(require 'kzk-window-management)
(require 'kzk-shell-hacks)
(require 'init-dired)
(require 'init-compilation)
(require 'init-company)
(require 'init-dev-env)
(require 'init-spell)
(require 'init-pdf)
(require 'init-org)
(require 'init-latex)
(require 'init-markdown)
;; }}}

;;; {{ Custom File -- Load here, where it originally was
(load custom-file)
;;; }}

(require 'server)
(unless (server-running-p) (server-start))
(tool-bar-mode -1)

;; }}}
